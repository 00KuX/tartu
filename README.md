<<<<<<< README.md
# TarTu

## Sistema de Turnos para Taller 

## Requerimientos :wrench:

- Docker version 24.0.5
- Docker-Compose v2.17.2

## Iniciar :zap:

1. Clonar repositorio
2. Configurar variables de entorno

```bash
copiar  .env.example  en  .env
```

3. Iniciar contenedores por primera vez o cuando se modifique el requirements.txt

```bash
docker compose up --build
```

4. Iniciar contenedores en segundo plano

```bash
docker compose up -d
```


5. Entrar al contenedor y correr test

```bash
docker exec -it turnos-web-1 bash
```
si no funciona
```
docker exec -it turnos-web-1 sh
```

8. Correr test (dentro del contenedor)

```bash
python manage.py test
```
