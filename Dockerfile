FROM python:3.7-alpine3.17
ENV PYTHONUNBUFFERED 1
RUN mkdir /code
WORKDIR /code
COPY requeriments.txt /code/
RUN pip install -r requeriments.txt
COPY . /code/